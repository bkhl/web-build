#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Build script for Zola based web sites.
"""

from plumbum import cli, local
from plumbum.cmd import tidy, xmlstarlet, zola  # pylint: disable=import-error


class Builder(cli.Application):
    """
    Build and format/clean a Zola based web site.
    """

    _zola_args = []
    _output_dir = local.cwd / "public"

    @cli.switch(["-u", "--base-url"], str)
    def base_url(self, url):
        """
        Pick a base URL different from the Zola default.
        """

        self._zola_args.extend(["--base-url", url])

    @cli.switch(["-o", "--output-dir"], str)
    def output_dir(self, path):
        """
        Pick an output directory other than the Zola default.
        """

        self._output_dir = local.path(path)
        self._zola_args.extend(["--output-dir", path])

    def _public_files_by_extension(self, *extensions):
        for filepath in self._output_dir.walk(
            filter=lambda f: f.is_file()
            and f.suffix in ("." + e for e in extensions)
            and not self._is_static_file(f)
        ):
            yield filepath

    def _is_static_file(self, filepath):
        return (local.cwd / "static" / filepath.relative_to(self._output_dir)).exists()

    def _clean_html(self):
        files = list(self._public_files_by_extension("html"))
        tidy(
            "--quiet",
            "no",
            "--show-warnings",
            "no",
            "--tidy-mark",
            "no",
            "--vertical-space",
            "no",
            "--wrap",
            "0",
            "--write-back",
            "yes",
            *files,
            retcode=(0, 1)
        )

    def _clean_xml(self):
        for filepath in self._public_files_by_extension("xml"):
            xml = xmlstarlet(
                "fo",
                "--noindent",
                "--nocdata",
                "--nsclean",
                "--encode",
                "utf-8",
                filepath,
            )
            with filepath.open("w") as file_object:
                file_object.write(xml)

    def main(self, *_args):
        zola("build", *self._zola_args)
        self._clean_html()
        self._clean_xml()


if __name__ == "__main__":
    Builder.run()
