FROM fedora:31

RUN dnf install -y python3-plumbum tidy xmlstarlet

RUN curl --silent --show-error \
        --location 'https://github.com/getzola/zola/releases/download/v0.9.0/zola-v0.9.0-x86_64-unknown-linux-gnu.tar.gz' \
        | tar xzf - -C /usr/local/bin/
