#!/usr/bin/env python3

"""
Script for fetching and deploying a web site that has been published as a
GitLab artifact.
"""

import argparse
import io
import os
import zipfile

import requests


class ArtifactNotFoundException(Exception):
    """
    Exception raised if artifact is not found on GitLab server.
    """


class CurrentEtagNotFoundException(Exception):
    """
    Exception raised if we can not work out the ETag for the version of the
    website that's currently deployed.
    """


class InvalidArtifactException(Exception):
    """
    Exception raised if the downloaded artifact can not be unpacked or is
    otherwise invalid.
    """


ARTIFACT_DIRECTORY_PREFIX = "public/"


def main():
    """
    Script main function.
    """

    args = _parse_args()

    artifact_url = args.artifact_url
    destination_dir = args.destination_dir

    etag_filename = "{destination_dir}/.etag".format(destination_dir=destination_dir)

    session = requests.Session()

    try:
        remote_etag = _get_remote_etag(session, artifact_url)
    except ArtifactNotFoundException:
        return

    try:
        current_etag = _get_current_etag(etag_filename)
    except CurrentEtagNotFoundException:
        pass
    else:
        if remote_etag == current_etag:
            return

    try:
        _fetch_website(
            session=session,
            artifact_url=artifact_url,
            destination_dir=destination_dir,
            etag_filename=etag_filename,
        )
    except ArtifactNotFoundException:
        return


def _parse_args():
    """
    Parse command line arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("artifact_url", help="HTTP URL to artifact")
    parser.add_argument(
        "destination_dir", help="path of directory to deploy website into"
    )

    return parser.parse_args()


def _remove_artifact_prefix(path: str):
    return path[len(ARTIFACT_DIRECTORY_PREFIX) :]


def _is_ok_response(response: requests.Response):
    """
    Verify that HEAD/GET response looks valid.
    """

    return (
        response.status_code == 200
        and "ETag" in response.headers
        and response.headers.get("Content-Type", None) == "application/zip"
    )


def _is_ok_archive(archive: zipfile.ZipFile):
    """
    Verify that archive (ZipFile object) looks valid.
    """

    return all(
        name.startswith(ARTIFACT_DIRECTORY_PREFIX) for name in archive.namelist()
    )


def _get_remote_etag(session: requests.Session, artifact_url: str):
    """
    Get ETag of remote artifact, using a HEAD request, also checking that it
    looks like an artifact is currently available.
    """

    response = session.head(artifact_url, allow_redirects=True)

    if _is_ok_response(response):
        return response.headers["ETag"]

    raise ArtifactNotFoundException


def _get_current_etag(etag_filename: str):
    """
    Get ETag stored with currently deployed website.
    """

    if os.path.isfile(etag_filename):
        with open(etag_filename) as etag_file:
            return etag_file.read().rstrip()
    else:
        raise CurrentEtagNotFoundException


def _fetch_website(
    session: requests.Session, artifact_url, destination_dir: str, etag_filename: str
):
    """
    Fetch and deploy the website.
    """

    response = session.get(artifact_url, allow_redirects=True)

    if not _is_ok_response(response):
        raise ArtifactNotFoundException

    with zipfile.ZipFile(io.BytesIO(response.content)) as archive:
        if not _is_ok_archive(archive):
            raise InvalidArtifactException

        _extract_files(archive=archive, destination_dir=destination_dir)
        _delete_files(archive=archive, destination_dir=destination_dir)
        _save_etag(etag_filename=etag_filename, etag=response.headers["ETag"])


def _get_target_path(destination_dir: str, path: str) -> str:
    return "{destination_dir}/{path}".format(
        destination_dir=destination_dir,
        path=_remove_artifact_prefix(path).rstrip(os.path.sep),
    )


def _extract_files(archive: zipfile.ZipFile, destination_dir: str):
    """
    Extract files to target directory.
    """

    for info in archive.infolist():
        target_path = _get_target_path(destination_dir, info.filename)
        if info.filename.endswith(os.path.sep):
            if os.path.exists(target_path):
                if not os.path.isdir(target_path):
                    os.remove(target_path)
                    os.mkdir(target_path)
            else:
                os.mkdir(target_path)
        else:
            with archive.open(info) as source_file, open(
                target_path, "wb"
            ) as destination_file:
                destination_file.write(source_file.read())


def _delete_files(archive: zipfile.ZipFile, destination_dir: str):
    """
    Delete any files and directories that was not included in the artifact.
    """

    keep_files = set(
        _get_target_path(destination_dir, path) for path in archive.namelist()
    )

    for path, directory_names, file_names in os.walk(destination_dir, topdown=False):
        for file_name in file_names:
            file_path = os.path.join(path, file_name)
            if file_path not in keep_files:
                os.remove(file_path)

        for directory_name in directory_names:
            directory_path = os.path.join(path, directory_name)
            if directory_path not in keep_files:
                os.rmdir(directory_path)


def _save_etag(etag_filename: str, etag: str):
    """
    Save ETag to a ".etag" file in the destination directory, for comparison
    to the artifact ETag in the next iteration.
    """

    with open(etag_filename, "w") as etag_file:
        etag_file.write("{etag}\n".format(etag=etag))


if __name__ == "__main__":
    main()
