# Web site build/deploy scripts

A couple of scripts used to deploy some of my websites, including:

* [elektrubadur.se](https://elektrubadur.se) — [repository](https://gitlab.com/bkhl/elektrubadur.se)
* [upsala.fandom.se](https://upsala.fandom.se) — [repository](https://gitlab.com/upsalafandom/upsala.fandom.se)

## Scripts

### `build_website.py`

Build website using [Zola](), then clean up the resulting HTML/XML using [HTML
Tidy](http://www.html-tidy.org/) and [xmlstarlet](http://xmlstar.sourceforge.net/).

### `fetch_website.py`

Fetch an artifact containing the built website from GitLab and deploy it,
unless the ETag of the artifact indicates that the same version was already
deployed.

This can run as a cronjob on the web server.
